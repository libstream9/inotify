#ifndef STREAM9_INOTIFY_INOTIFY_FWD_HPP
#define STREAM9_INOTIFY_INOTIFY_FWD_HPP

namespace stream9 {

class inotify;
class inotify_events;
class inotify_event_range;
class inotify_event_iterator;

} // namespace stream9

#endif // STREAM9_INOTIFY_INOTIFY_FWD_HPP
