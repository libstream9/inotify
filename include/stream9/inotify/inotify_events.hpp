#ifndef STREAM9_INOTIFY_EVENTS_HPP
#define STREAM9_INOTIFY_EVENTS_HPP

#include "inotify_event_iterator.hpp"

#include <memory>

namespace stream9 {

class inotify_events
{
public:
    using const_iterator = inotify_event_iterator;

public:
    inotify_events(std::unique_ptr<char[]> buf, size_t size)
        : m_buf { std::move(buf) }
        , m_size { size }
    {}

    // iterator
    const_iterator begin() const
    {
        return { &m_buf[0], &m_buf[m_size] };
    }

    const_iterator end() const
    {
        return {};
    }

    // query
    bool empty() const { return m_size == 0; }

private:
    std::unique_ptr<char[]> m_buf;
    size_t m_size;
};

} // namespace stream9

#endif // STREAM9_INOTIFY_EVENTS_HPP
