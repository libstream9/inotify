#ifndef STREAM9_INOTIFY_EVENT_ITERATOR_HPP
#define STREAM9_INOTIFY_EVENT_ITERATOR_HPP

#include <cassert>
#include <iterator>

#include <sys/inotify.h>

#include <boost/iterator/iterator_facade.hpp>

namespace stream9 {

class inotify_event_iterator : public boost::iterator_facade<
                                            inotify_event_iterator,
                                            ::inotify_event const,
                                            std::forward_iterator_tag >
{
public:
    inotify_event_iterator() = default;
    inotify_event_iterator(char const* const begin, char const* const end)
        : m_it { begin }
        , m_end { end }
    {}

private:
    friend class boost::iterator_core_access;

    ::inotify_event const& dereference() const
    {
        return *reinterpret_cast<::inotify_event const*>(m_it);
    }

    void increment()
    {
        m_it += (sizeof(::inotify_event) +
                 reinterpret_cast<::inotify_event const*>(m_it)->len );

        assert(m_it <= m_end);
    }

    bool equal(inotify_event_iterator const& other) const
    {
        if (!other.m_it) {
            return m_it == m_end;
        }
        else {
            return m_it == other.m_it
                && m_end == other.m_end;
        }
    }

private:
    char const* m_it = nullptr;
    char const* m_end = nullptr;
};

} // namespace stream9

#endif // STREAM9_INOTIFY_EVENT_ITERATOR_HPP
