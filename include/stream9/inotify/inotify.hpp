#ifndef STREAM9_INOTIFY_HPP
#define STREAM9_INOTIFY_HPP

#include "inotify_events.hpp"
#include "inotify_event_range.hpp"

#include <sys/inotify.h>

#include <string>

namespace stream9 {

/**
 * inotify: thin wrapper around linux inotify API
 */
class inotify
{
public:
    inotify();
    ~inotify() noexcept;

    inotify(inotify const&) = delete;
    inotify& operator=(inotify const&) = delete;

    inotify(inotify&&);
    inotify& operator=(inotify&&);

    // accessor
    int fd() const { return m_fd; }

    // command
    int add_watch(char const* path, uint32_t mask);
    void rm_watch(int wd);

    // query
    inotify_events      read();
    inotify_event_range read(char* buf, size_t buf_len);

    bool poll(int timeout) const;

private:
    int m_fd;
};

std::string mask_to_string(uint32_t mask);

} // namespace stream9

#endif // STREAM9_INOTIFY_HPP
