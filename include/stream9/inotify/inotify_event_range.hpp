#ifndef STREAM9_INOTIFY_EVENT_RANGE_HPP
#define STREAM9_INOTIFY_EVENT_RANGE_HPP

#include "inotify_event_iterator.hpp"

namespace stream9 {

class inotify_event_range
{
public:
    using const_iterator = inotify_event_iterator;

public:
    inotify_event_range(char const* buf, size_t len)
        : inotify_event_range { buf, buf + len }
    {}

    inotify_event_range(char const* begin, char const* end)
        : m_begin { begin }
        , m_end { end }
    {}

    // iterator
    const_iterator begin() const
    {
        return { m_begin, m_end };
    }

    const_iterator end() const
    {
        return {};
    }

    // query
    bool empty() const { return m_begin == m_end; }

private:
    char const* m_begin; // non-null
    char const* m_end; // non-null
};

} // namespace stream9

#endif // STREAM9_INOTIFY_EVENT_RANGE_HPP
