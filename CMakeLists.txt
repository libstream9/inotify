cmake_minimum_required(VERSION 3.16)

project(inotify CXX)

add_compile_options(-Wall -Wextra -pedantic -Wconversion -Wsign-conversion)

add_subdirectory(src)

if(BUILD_EXAMPLE)
    add_subdirectory(example)
endif()
