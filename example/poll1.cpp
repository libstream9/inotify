#include <stream9/inotify/inotify.hpp>

#include "utility.hpp"

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <thread>

namespace fs = std::filesystem;

static void
watch_dir(fs::path const& dir_path, bool& stop)
{
    stream9::inotify inotify;

    inotify.add_watch(dir_path.c_str(), IN_ALL_EVENTS);

    while (!stop) {
        if (inotify.poll(1000)) {
            int i = 0;
            for (auto const& ev: inotify.read()) {
                std::cout << ++i << "----------\n"
                          << "wd:     " << ev.wd << '\n'
                          << "mask:   " << mask_to_string(ev.mask) << '\n'
                          << "cookie: " << std::hex << ev.cookie << '\n'
                          << "len:    " << std::dec << ev.len << '\n'
                          << "name:   " << ev.name << '\n'
                          ;
            }
        }
    }
}

int main()
{
    fs::path dir_path { "blocking_dir" };

    if (fs::exists(dir_path)) {
        fs::remove_all(dir_path);
    }

    fs::create_directory(dir_path);

    bool stop = false;
    std::thread t { [&] { watch_dir(dir_path, stop); } };

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(1s);

    std::ofstream fs;
    fs.open(dir_path / "file1");
    fs << "foo";
    fs.close();

    fs.open(dir_path / "file2");
    fs << "bar";
    fs.close();

    fs::remove_all(dir_path);

    std::this_thread::sleep_for(1s);
    stop = true;

    t.join();
}
