/*
 * asio2
 * - dont use thread
 * - use ASIO stackless coroutine
 * - both waiting and reading is done with ASIO
 */
#include "utility.hpp"

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>

#include <boost/asio/coroutine.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/system_timer.hpp>
#include <boost/asio/yield.hpp>
#include <boost/system/system_error.hpp>

#include <stream9/inotify/inotify.hpp>

namespace fs = std::filesystem;
namespace asio = boost::asio;

class watcher : asio::coroutine
{
public:
    watcher(asio::io_context& ioc, fs::path const& dir_path)
        : m_ioc { &ioc }
        , m_desc { std::make_shared<asio::posix::stream_descriptor>(*m_ioc) }
        , m_inotify { std::make_shared<stream9::inotify>() }
        , m_buf { std::make_shared<std::vector<char>>() }
    {
        m_buf->resize(4096);

        m_inotify->add_watch(dir_path.c_str(), IN_ALL_EVENTS);
        m_desc->assign(m_inotify->fd());

        (*this)();
    }

    // asio stackless coroutine needs to be copyable
    watcher(watcher const&) = default;
    watcher& operator=(watcher const&) = default;

    void operator()(boost::system::error_code ec = {}, size_t const n = 0)
    {
        if (ec) throw boost::system::system_error { ec };

        reenter (this) while (true) {
            std::cout << "watcher: wait for events\n";

            yield m_desc->async_read_some(asio::buffer(*m_buf), *this);

            auto i = 0;
            stream9::inotify_event_range const events { m_buf->data(), n };
            for (auto const& ev: events) {
                std::cout << ++i << "----------\n"
                          << "wd:     " << ev.wd << '\n'
                          << "mask:   " << mask_to_string(ev.mask) << '\n'
                          << "cookie: " << std::hex << ev.cookie << '\n'
                          << "len:    " << std::dec << ev.len << '\n'
                          << "name:   " << ev.name << '\n'
                          ;
            }
        }
    }

private:
    asio::io_context* m_ioc;
    std::shared_ptr<asio::posix::stream_descriptor> m_desc;
    std::shared_ptr<stream9::inotify> m_inotify;
    std::shared_ptr<std::vector<char>> m_buf;
};

class actor : asio::coroutine
{
public:
    actor(asio::io_context& ioc, fs::path const& dir_path)
        : m_ioc { &ioc }
        , m_dir_path { &dir_path }
        , m_timer { std::make_shared<asio::system_timer>(*m_ioc) }
    {
        (*this)();
    }

    actor(actor const&) = default;
    actor& operator=(actor const&) = default;

    void operator()(boost::system::error_code ec = {}, size_t const n = 0)
    {
        using namespace std::chrono_literals;
        (void)n;

        if (ec) throw boost::system::system_error { ec };

        std::ofstream fs;

        reenter (this) {
            std::cout << "actor: wait for 1s\n";

            m_timer->expires_from_now(1s);
            yield m_timer->async_wait(*this);

            std::cout << "actor: start doing thing\n";

            fs.open(*m_dir_path / "file1");
            fs << "foo";
            fs.close();

            fs.open(*m_dir_path / "file2");
            fs << "bar";
            fs.close();

            fs::remove_all(*m_dir_path);

            std::cout << "actor: wait for 1s and stop\n";

            m_timer->expires_from_now(1s);
            yield m_timer->async_wait(*this);

            m_ioc->stop();
        }
    }

private:
    asio::io_context* m_ioc; // non-null
    fs::path const*  m_dir_path; // non-null
    std::shared_ptr<asio::system_timer> m_timer;
};

int main()
{
    fs::path dir_path { "test_dir" };

    if (fs::exists(dir_path)) {
        fs::remove_all(dir_path);
    }

    fs::create_directory(dir_path);

    asio::io_context ioc;

    watcher w { ioc, dir_path };
    actor a { ioc, dir_path };

    ioc.run();
}
