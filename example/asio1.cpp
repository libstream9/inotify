/*
 * asio1
 * - not use thread
 * - use ASIO to wait for events
 * - read events with inotify::read
 */
#include "utility.hpp"

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>

#include <boost/asio/io_context.hpp>
#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/system_timer.hpp>
#include <boost/system/system_error.hpp>

#include <stream9/inotify/inotify.hpp>

namespace fs = std::filesystem;
namespace asio = boost::asio;

class watcher
{
public:
    watcher(asio::io_context& ioc, fs::path const& dir_path)
        : m_ioc { ioc }
        , m_desc { m_ioc }
    {
        m_inotify.add_watch(dir_path.c_str(), IN_ALL_EVENTS);
        m_desc.assign(m_inotify.fd());

        wait();
    }

private:
    void wait()
    {
        std::cout << "watcher: wait for events\n";

        m_desc.async_wait(m_desc.wait_read,
            [this](auto ec) {
                if (ec) throw boost::system::system_error { ec };

                read();
            } );
    }

    void read()
    {
        std::cout << "watcher: read events\n";

        auto i = 0;
        for (auto const& ev: m_inotify.read()) {
            std::cout << ++i << "----------\n"
                      << "wd:     " << ev.wd << '\n'
                      << "mask:   " << mask_to_string(ev.mask) << '\n'
                      << "cookie: " << std::hex << ev.cookie << '\n'
                      << "len:    " << std::dec << ev.len << '\n'
                      << "name:   " << ev.name << '\n'
                      ;
        }

        wait();
    }

private:
    asio::io_context& m_ioc;
    asio::posix::stream_descriptor m_desc;
    stream9::inotify m_inotify;
};

class actor
{
public:
    actor(asio::io_context& ioc, fs::path const& dir_path)
        : m_ioc { ioc }
        , m_dir_path { dir_path }
        , m_timer { m_ioc }
    {
        wait();
    }

private:
    void wait()
    {
        using namespace std::chrono_literals;

        std::cout << "actor: wait for 1s\n";

        m_timer.expires_from_now(1s);
        m_timer.async_wait(
            [this](auto ec) {
                if (ec) throw boost::system::system_error { ec };

                do_the_thing();
            } );
    }

    void do_the_thing()
    {
        std::cout << "actor: start doing thing\n";

        std::ofstream fs;
        fs.open(m_dir_path / "file1");
        fs << "foo";
        fs.close();

        fs.open(m_dir_path / "file2");
        fs << "bar";
        fs.close();

        fs::remove_all(m_dir_path);

        stop_after_1s();
    }

    void stop_after_1s()
    {
        using namespace std::chrono_literals;

        std::cout << "actor: wait for 1s and stop\n";

        m_timer.expires_from_now(1s);
        m_timer.async_wait(
            [this](auto ec) {
                if (ec) throw boost::system::system_error { ec };

                m_ioc.stop();
            } );
    }

private:
    asio::io_context& m_ioc;
    fs::path const& m_dir_path;
    asio::system_timer m_timer;
};

int main()
{
    fs::path dir_path { "test_dir" };

    if (fs::exists(dir_path)) {
        fs::remove_all(dir_path);
    }

    fs::create_directory(dir_path);

    asio::io_context ioc;
    watcher w { ioc, dir_path };
    actor a { ioc, dir_path };

    ioc.run();
}
