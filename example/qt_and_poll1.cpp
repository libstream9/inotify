#include <stream9/inotify/inotify.hpp>

#include "utility.hpp"

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <thread>

#include <QCoreApplication>
#include <QObject>
#include <QTimer>
#include <QDebug>

namespace fs = std::filesystem;

static void
print_thread_id(std::string_view const name)
{
    std::cout << name << ": " << std::hex << std::this_thread::get_id() << "\n";
}

static void
init(fs::path const& dir_path)
{
    if (fs::exists(dir_path)) {
        fs::remove_all(dir_path);
    }

    fs::create_directory(dir_path);
}

class inotify_observer
{
public:
    virtual ~inotify_observer() {}

    virtual void on_events(stream9::inotify_events) = 0;
};

class watcher
{
public:
    watcher(fs::path const& dir_path, inotify_observer& o)
        : m_dir_path { dir_path }
        , m_observer { o }
    {}

    ~watcher()
    {
        if (m_thread) {
            m_stop = true;
            m_thread->join();
        }
    }

    void run()
    {
        if (m_thread) return;

        m_thread = std::make_unique<std::thread>(&watcher::run_, this);
    }

    void stop()
    {
        std::cout << "watcher: stop\n";
        m_stop = true;
    }

private:
    void run_()
    {
        stream9::inotify inotify;
        inotify.add_watch(m_dir_path.c_str(), IN_ALL_EVENTS);

        std::cout << "watcher: wait for events\n";
        while (!m_stop) {
            if (inotify.poll(1000)) {
                auto events = inotify.read();
                m_observer.on_events(std::move(events));
            }
        }
    }

private:
    fs::path const& m_dir_path;
    inotify_observer& m_observer;
    bool m_stop = false;
    std::unique_ptr<std::thread> m_thread;
};

class Actor : public QObject
{
    Q_OBJECT
public:
    Actor(fs::path const& dir_path)
        : m_dir_path { dir_path }
    {
        QTimer::singleShot(1000, [&] { this->do_the_thing(); });
    }

private:
    void do_the_thing()
    {
        qInfo() << "Actor: start doing thing";

        std::ofstream fs;
        fs.open(m_dir_path / "file1");
        fs << "foo";
        fs.close();

        fs.open(m_dir_path / "file2");
        fs << "bar";
        fs.close();

        fs::remove_all(m_dir_path);

        QTimer::singleShot(1000, [] { qApp->exit(); });
    }

private:
    fs::path m_dir_path;
};

// QMetaType requires type to be copyable
using INotifyEventsPtr = std::shared_ptr<stream9::inotify_events>;

Q_DECLARE_METATYPE(INotifyEventsPtr)

class Receiver : public QObject, inotify_observer
{
    Q_OBJECT
public:
    Receiver(fs::path const& dir_path)
        : m_watcher { dir_path, *this }
    {
        qRegisterMetaType<INotifyEventsPtr>();

        this->connect(this, &Receiver::iNotifyEvents,
                      this, &Receiver::onINotifyEvents, Qt::QueuedConnection);

        m_watcher.run();
    }

    ~Receiver()
    {
        m_watcher.stop();
    }

    void on_events(stream9::inotify_events e) override
    {
        print_thread_id(__func__);

        // send events across Qt thread via queued connection
        auto events = std::make_shared<stream9::inotify_events>(std::move(e));
        Q_EMIT iNotifyEvents(events);
    }

private:
    Q_SIGNAL void iNotifyEvents(INotifyEventsPtr const&);

    Q_SLOT void onINotifyEvents(INotifyEventsPtr const& events)
    {
        print_thread_id(__func__);

        int i = 0;
        for (auto const& ev: *events) {
            std::cout << ++i << "----------\n"
                      << "wd:     " << ev.wd << '\n'
                      << "mask:   " << mask_to_string(ev.mask) << '\n'
                      << "cookie: " << std::hex << ev.cookie << '\n'
                      << "len:    " << std::dec << ev.len << '\n'
                      << "name:   " << ev.name << '\n'
                      ;
        }
    }

private:
    watcher m_watcher;
};

#include "qt_and_poll1.moc"

int main(int argc, char* argv[])
{
    QCoreApplication app { argc, argv };

    fs::path dir_path { "test_dir" };

    init(dir_path);

    Receiver r { dir_path };
    Actor a { dir_path };

    print_thread_id(__func__);
    app.exec();
}
