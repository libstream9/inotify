#ifndef STREAM9_INOTIFY_EXAMPLE_UTILITY_HPP
#define STREAM9_INOTIFY_EXAMPLE_UTILITY_HPP

#include <string>
#include <sstream>

#include <sys/inotify.h>

static std::string
mask_to_string(uint32_t const mask)
{
    std::ostringstream oss;

    if (mask & IN_ACCESS) oss << "IN_ACCESS ";
    if (mask & IN_MODIFY) oss << "IN_MODIFY ";
    if (mask & IN_ATTRIB) oss << "IN_ATTRIB ";
    if (mask & IN_CLOSE_WRITE) oss << "IN_CLOSE_WRITE ";
    if (mask & IN_CLOSE_NOWRITE) oss << "IN_CLOSE_NOWRITE ";
    if (mask & IN_OPEN) oss << "IN_OPEN ";
    if (mask & IN_MOVED_FROM) oss << "IN_MOVED_FROM ";
    if (mask & IN_MOVED_TO) oss << "IN_MOVED_TO ";
    if (mask & IN_CREATE) oss << "IN_CREATE ";
    if (mask & IN_DELETE) oss << "IN_DELETE ";
    if (mask & IN_DELETE_SELF) oss << "IN_DELETE_SELF ";
    if (mask & IN_MOVE_SELF) oss << "IN_MOVE_SELF ";
    if (mask & IN_UNMOUNT) oss << "IN_UNMOUNT ";
    if (mask & IN_Q_OVERFLOW) oss << "IN_Q_OVERFLOW ";
    if (mask & IN_IGNORED) oss << "IN_IGNORED ";
    if (mask & IN_ISDIR) oss << "IN_ISDIR ";
    if (mask & IN_Q_OVERFLOW) oss << "IN_Q_OVERFLOW ";
    if (mask & IN_UNMOUNT) oss << "IN_UNMOUNT ";

    return oss.str();
}

#endif // STREAM9_INOTIFY_EXAMPLE_UTILITY_HPP
