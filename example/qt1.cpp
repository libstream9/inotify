#include "utility.hpp"

#include <filesystem>
#include <fstream>

#include <QCoreApplication>
#include <QDebug>
#include <QObject>
#include <QSocketNotifier>
#include <QTimer>

#include <stream9/inotify/inotify.hpp>

namespace fs = std::filesystem;

class Watcher : public QObject
{
public:
    Watcher(fs::path const& dir_path)
        : m_notifier { m_inotify.fd(), QSocketNotifier::Read }
    {
        m_inotify.add_watch(dir_path.c_str(), IN_ALL_EVENTS);

        this->connect(&m_notifier, &QSocketNotifier::activated,
                      [this] { this->read(); });
        qInfo() << "Watcher: wait for events";
    }

private:
    void read()
    {
        qInfo() << "Watcher: read events\n";

        auto i = 0;
        QTextStream ts { stdout };
        for (auto const& ev: m_inotify.read()) {
            ts << ++i << "----------\n"
               << "wd:     " << ev.wd << '\n'
               << "mask:   " << mask_to_string(ev.mask).c_str() << '\n'
               << "cookie: " << Qt::hex << ev.cookie << '\n'
               << "len:    " << Qt::dec << ev.len << '\n'
               << "name:   " << ev.name << '\n'
               ;
        }
    }

private:
    stream9::inotify m_inotify;
    QSocketNotifier m_notifier;
};

class Actor : public QObject
{
public:
    Actor(fs::path const& dir_path)
        : m_dir_path { dir_path }
    {
        QTimer::singleShot(1000, [&] { this->do_the_thing(); });
    }

private:
    void do_the_thing()
    {
        qInfo() << "Actor: start doing thing";

        std::ofstream fs;
        fs.open(m_dir_path / "file1");
        fs << "foo";
        fs.close();

        fs.open(m_dir_path / "file2");
        fs << "bar";
        fs.close();

        fs::remove_all(m_dir_path);

        QTimer::singleShot(1000, [] { qApp->exit(); });
    }

private:
    fs::path m_dir_path;
};


int main(int argc, char* argv[])
{
    QCoreApplication app { argc, argv };

    fs::path dir_path { "test_dir" };

    if (fs::exists(dir_path)) {
        fs::remove_all(dir_path);
    }

    fs::create_directory(dir_path);

    Watcher w { dir_path };
    Actor a { dir_path };

    app.exec();
}
