#include <stream9/inotify/inotify.hpp>

#include <memory>
#include <sstream>
#include <system_error>

#include <poll.h>
#include <sys/ioctl.h>

namespace stream9 {

static auto
make_errno_error()
{
    return std::system_error(
        std::make_error_code(static_cast<std::errc>(errno)) );
}

inotify::
inotify()
    : m_fd { ::inotify_init() }
{
    if (m_fd == -1) {
        throw make_errno_error();
    }
}

inotify::
~inotify()
{
    if (m_fd != -1) {
        ::close(m_fd);
    }
}

inotify::
inotify(inotify&& other)
{
    m_fd = other.m_fd;
    other.m_fd = -1;
}

inotify& inotify::
operator=(inotify&& other)
{
    m_fd = other.m_fd;
    other.m_fd = -1;

    return *this;
}

int inotify::
add_watch(char const* const path, uint32_t const mask)
{
    auto const wd = ::inotify_add_watch(m_fd, path, mask);
    if (wd == -1) {
        throw make_errno_error();
    }

    return wd;
}

void inotify::
rm_watch(int const wd)
{
    if (::inotify_rm_watch(m_fd, wd) != 0) {
        throw make_errno_error();
    }
}

inotify_events inotify::
read()
{
    int avail_bytes = 0;
    ::ioctl(m_fd, FIONREAD, &avail_bytes);

    auto const buf_size = static_cast<size_t>(std::max(avail_bytes, 4096));

    auto buf = std::make_unique<char[]>(buf_size);

    auto const len = ::read(m_fd, buf.get(), buf_size);
    if (len == -1) throw make_errno_error();
    assert(len >= 0);

    return { std::move(buf), static_cast<size_t>(len) };
}

inotify_event_range inotify::
read(char* const buf, size_t const buf_len)
{
    auto const read_len = ::read(m_fd, buf, buf_len);
    if (read_len == -1) throw make_errno_error();
    assert(read_len >= 0);

    return { buf, static_cast<size_t>(read_len) };
}

bool inotify::
poll(int const timeout) const
{
    ::pollfd fds[1];
    fds[0].fd = m_fd;
    fds[0].events = POLLIN;

    auto const num = ::poll(fds, 1, timeout);
    if (num == -1) throw make_errno_error();

    return num > 0;
}

std::string
mask_to_string(uint32_t const mask)
{
    std::ostringstream oss;

    if (mask & IN_ACCESS) oss << "IN_ACCESS ";
    if (mask & IN_MODIFY) oss << "IN_MODIFY ";
    if (mask & IN_ATTRIB) oss << "IN_ATTRIB ";
    if (mask & IN_CLOSE_WRITE) oss << "IN_CLOSE_WRITE ";
    if (mask & IN_CLOSE_NOWRITE) oss << "IN_CLOSE_NOWRITE ";
    if (mask & IN_OPEN) oss << "IN_OPEN ";
    if (mask & IN_MOVED_FROM) oss << "IN_MOVED_FROM ";
    if (mask & IN_MOVED_TO) oss << "IN_MOVED_TO ";
    if (mask & IN_CREATE) oss << "IN_CREATE ";
    if (mask & IN_DELETE) oss << "IN_DELETE ";
    if (mask & IN_DELETE_SELF) oss << "IN_DELETE_SELF ";
    if (mask & IN_MOVE_SELF) oss << "IN_MOVE_SELF ";
    if (mask & IN_UNMOUNT) oss << "IN_UNMOUNT ";
    if (mask & IN_Q_OVERFLOW) oss << "IN_Q_OVERFLOW ";
    if (mask & IN_IGNORED) oss << "IN_IGNORED ";
    if (mask & IN_ISDIR) oss << "IN_ISDIR ";
    if (mask & IN_Q_OVERFLOW) oss << "IN_Q_OVERFLOW ";
    if (mask & IN_UNMOUNT) oss << "IN_UNMOUNT ";

    return oss.str();
}

} // namespace stream9
